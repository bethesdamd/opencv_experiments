# Foreground extraction
# From: https://pythonprogramming.net/grabcut-foreground-extraction-python-opencv-tutorial/?completed=/template-matching-python-opencv-tutorial/
# This seems to work fairly well, however it is slow, taking about
# a second to compute, which would not be good for video.  Image
# size is about 800 x 530

import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('./images/soccer.jpg')
mask = np.zeros(img.shape[:2],np.uint8)

bgdModel = np.zeros((1,65),np.float64)
fgdModel = np.zeros((1,65),np.float64)

rect = (220,50,300,300)
cv2.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv2.GC_INIT_WITH_RECT)
mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
img = img*mask2[:,:,np.newaxis]

plt.imshow(img)
plt.colorbar()
plt.show()

