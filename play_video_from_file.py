# Read video from a file and display it
# Works with mp4 as shown, and apparently handles other formats like avi

# from https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html
# You can generate a test mp4 file using an iPhone as I recall



import numpy as np
import cv2

cap = cv2.VideoCapture('/Users/david/Documents/orange-target.mp4')

while(cap.isOpened()):
    ret, frame = cap.read()

    # Now do whatever processing you need on the frame

    # Convert to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',gray)

    # Stop the program with 'q' keystroke.  (Must have mouse focus over video window for this to work)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
