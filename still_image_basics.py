# I'm running this on a Mac with anaconda python installed
# See the following re installation details:
#  https://anaconda.org/menpo/opencv2
# if you get an error on the import, could be: https://github.com/ContinuumIO/anaconda-issues/issues/826 
# To kill the image display window, press 'q' while the focus is on the window

import numpy as np
import cv2 # import the opencv library

print(cv2.__version__)   # this will print the version of your opencv2

cv2.namedWindow('imageWindow')
# Load an color image in grayscale
#img = cv2.imread('/Users/david/opencv_python3/images/pitcher_medium.jpg',0)
# img = cv2.imread('/Users/david/opencv_python3/images/pitcher_medium.jpg')
img = cv2.imread('./images/pitcher_medium.jpg')
cv2.imshow('imageWindow',img)
wait = True
while wait:
  wait = cv2.waitKey()=='q113' # hit q to exit

