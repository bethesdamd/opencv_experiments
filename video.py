# Capture some video from camera and display it
# From https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html#display-video
# press 'q' key to close video window when it's in the foreground

import numpy as np
import cv2

# Define the lower and upper boundaries of the color we need
# This is not easy to do, I'll try to explain some techniques in another document
lower = np.array([85,50,50])  # HSV
upper = np.array([93,255,255])

# A kernal is like a matrix that is used in the morphology operation
# See https://en.wikipedia.org/wiki/Kernel_(image_processing) if interested
kernel = np.ones((6,6),np.uint8)

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, img = cap.read()

    # Our operations on the frame come here
    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    # cv2.imshow('frame', img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # Convert the image from RGB to HSV color space.  This is required for the next operation.
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # Create a new image that contains yellow where the color was detected, otherwise purple
    res = cv2.inRange(hsv, lower, upper)

    # The morphological 'open' operation is described here:
    # https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html
    # It helps remove noise and jagged edges, note how the stray speckles are removed!
    opened = cv2.morphologyEx(res, cv2.MORPH_OPEN, kernel)
   
   # Blurring operation helps forthcoming findContours operation work better
    blur = cv2.blur(opened, (3,3))
    
    # Find contours
    (_, cnts, _) = cv2.findContours(blur.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)    

    # Sometimes the contours operation will find more than two contours
    # But if we did all our preliminary operations properly, then the two contours we need will be
    # the *largest* contours in the set of contours.  The sorted operation below sorts
    # the cnts array of contours by area, so the first two contours will be the largest
  
    contours = sorted(cnts, key = cv2.contourArea, reverse = True)
    
    if len(contours) > 0:
        cnt1 = contours[0]
        # cnt2 = sorted(cnts, key = cv2.contourArea, reverse = True)[1]
        
        # Draw a minimum area rectangle around each contour
        rect1 = np.int32(cv2.boxPoints(cv2.minAreaRect(cnt1)))
        # rect2 = np.int32(cv2.boxPoints(cv2.minAreaRect(cnt2)))
        
        # Draw the contours in red (255, 0, 0) on top of our original image
        cv2.drawContours(blur, [rect1], -1, (255, 0, 0), 2)
        # cv2.drawContours(blur, [rect2], -1, (255, 0, 0), 2)    
    
    cv2.imshow('frame', blur)

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()


